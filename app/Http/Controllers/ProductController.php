<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Product;
use App\Store;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $products = Product::all();
        return view('products.index')
        ->with('products',$products);
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $stores = Store::all();
        return view('products.create')
        ->with('stores',$stores);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $this->validate($request, [
            'name' => 'required',
            'description' => 'required'
        ]);
        $product = new Product;
        $product->name = $request->input('name');
        $product->status = $request->input('status');
        $product->description = $request->input('description');
        $product->store_id = $request->input('store_id');

        $product->save();

        return redirect('/products')->with('success','Product created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        $product = Product::find($id);

        $stores = Store::all();
        
        return view('products.edit')
        ->with('product', $product)
        ->with('stores', $stores);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $this->validate($request, [
            // 'title' => 'required'
        ]);
        $product = Product::find($id);
        $product->name = $request->input('name');
        $product->status = $request->input('status');
        $product->description = $request->input('description');
        $product->store_id = $request->input('store_id');
        $product->save();

        

        return redirect('/products')->with('success','product updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $product = Product::find($id);
        $product->delete();
        return redirect('/products')->with('error','Product Deleted');
    }
}
