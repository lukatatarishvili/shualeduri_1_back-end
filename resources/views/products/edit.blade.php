@extends('layouts.app')

@section('content')



<h1 style="text-align: center">Edit product</h1>

        <form method="post" action="{{ action("ProductController@update", $product->id) }}" >
            <div class="form-group">
                <label for="name">Product tname</label>
            <input type="text" value="{{$product->name}}" name="name" id="name" class="form-control" id="">
            </div>
            <div class="form-group">
                <label for="status">Status</label>
                <select class="form-control" name="status" id="status">
                    <option value="1">Works</option>
                    <option value="2">Amortized</option>
                  </select>
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <textarea name="description"  id="description" cols="30" class="form-control" rows="10">{{$product->description}}</textarea>
               
            </div>
            <div class="form-group">
                <label for="store_id">Store</label>
                <select class="form-control" name="store_id" id="status">
                    @foreach ($stores as $store)
                        <option value="{{$store->id}}">{{$store->store_name}}</option>
                    @endforeach
                  </select>
            </div>
            

            <input type="submit" class="btn btn-primary" value="submit">
            @method('PUT')
            @csrf
        </form>
    
        
@endsection