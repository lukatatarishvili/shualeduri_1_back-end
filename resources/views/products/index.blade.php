@extends('layouts.app')

@section('content')
<h1 style="text-align: center">Products</h1>
<a href="products/create" class="btn btn-primary" style="margin-bottom: 15px;">Create product</a>

    <div class="card">
        
                
        

        <table id="posts-table" class="table table-hover posts-table">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Product name</th>
                <th scope="col">Product Photo</th>
                <th scope="col">Status</th>
                <th scope="col">Description</th>
                <th scope="col">Store</th>
                <th scope="col">Edit</th>
                <th scope="col">Delete</th>
                
                
              </tr>
            </thead>
            <tbody>
                @foreach ($products as $product)
                <tr>
                <td>{{$product->id}}</td>
                <td>{{$product->name}}</td>
                <td><img src="https://lallahoriye.com.tirzee.com/wp-content/uploads/2019/04/Product_Lg_Type.jpg" alt="Product" width="100px;"></td>
                <td>{{$product->status}}</td>
                <td>{{$product->description}}</td>
                <td>{{$product->store_id}}</td>
                <td><a href="products/{{$product->id}}/edit" class="btn btn-primary">Edit</a></td>
                <td>  
                      <form method="POST" action="{{ action("ProductController@destroy", $product->id) }}" >
                          <input type="submit" value="Delete" class="btn btn-danger">

                          @method("DELETE")
                          @csrf
                      </form>
                </td> 
                    @csrf
                </tr>
                @endforeach
            </tbody>
          </table>
                
    </div>
        
    
@endsection
