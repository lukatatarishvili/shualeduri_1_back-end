@extends('layouts.app')

@section('content')
<h1 style="text-align: center">store</h1>
<a href="stores/create" class="btn btn-primary" style="margin-bottom: 15px;">Create store</a>

    <div class="card">
        
                
        

        <table id="posts-table" class="table table-hover posts-table">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">store name</th>
                <th scope="col">store Location</th>
                <th scope="col">Edit</th>
                <th scope="col">Delete</th>
                
                
              </tr>
            </thead>
            <tbody>
                @foreach ($store as $store)
                <tr>
                <td>{{$store->id}}</td>
                <td>{{$store->store_name}}</td>
                <td>{{$store->store_location}}</td>
                <td><a href="store/{{$store->id}}/edit" class="btn btn-primary">Edit</a></td>
                <td>  
                      <form method="POST" action="{{ action("StoreController@destroy", $store->id) }}" >
                          <input type="submit" value="Delete" class="btn btn-danger">

                          @method("DELETE")
                          @csrf
                      </form>
                </td> 
                    @csrf
                </tr>
                @endforeach
            </tbody>
          </table>
                
    </div>
        
    
@endsection
