@extends('layouts.app')

@section('content')



<h1 style="text-align: center">Add store</h1>

        <form method="post" action="{{ action("StoreController@store") }}">
            <div class="form-group">
                <label for="store_name">Store name</label>
                <input type="text" name="store_name" id="store_name" class="form-control" id="">
            </div>
            <div class="form-group">
                <label for="store_location">Store location</label>
                <input type="text" name="store_location" id="store_location" class="form-control" id="">
            </div>
           

            <input type="submit" class="btn btn-primary" value="submit">
            @csrf
        </form>
    
        
@endsection